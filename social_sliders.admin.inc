<?php

/**
 * @file
 * Functions that are only called on the admin pages.
 */

/**
 * Overriding system settings form.
 */
function social_sliders_system_settings_form($form, $automatic_defaults = TRUE) {
    $form['actions']['#type'] = 'container';
    $form['actions']['#attributes']['class'][] = 'form-actions';
    $form['actions']['#weight'] = 100;
    $form['actions']['submit'] = array('#type' => 'submit', '#value' => t('Save configuration'));

    if ($automatic_defaults) {
        $form = _system_settings_form_automatic_defaults($form);
    }

    if (!empty($_POST) && form_get_errors()) {
        drupal_set_message(t('The settings have not been saved because of the errors.'), 'error');
    }
    $form['#submit'][] = 'system_settings_form_submit';
    // By default, render the form using theme_system_settings_form().
    if (!isset($form['#theme'])) {
        $form['#theme'] = 'system_settings_form';
    }
    return $form;
}

/**
 * Module settings form.
 */
function social_sliders_admin_settings() {
    $form['social_sliders_fb_active'] = array(
        '#type' => 'checkbox',
        '#title' => t('Enable Facebook slider)'),
        '#default_value' => variable_get('social_sliders_fb_active'),
    );
    $form['social_sliders_fb_username'] = array(
        '#type' => 'textfield',
        '#title' => t('Facebook Username (ie http://www.facebook.com/username)'),
        '#description' => t('Go <a href="http://www.facebook.com/username">here</a> to setup a username for your facebook page.'),
        '#default_value' => variable_get('social_sliders_fb_username'),
    );
    $form['social_sliders_twit_active'] = array(
        '#type' => 'checkbox',
        '#title' => t('Enable Twitter slider)'),
        '#default_value' => variable_get('social_sliders_twit_active'),
    );

    $form['social_sliders_twit_username'] = array(
        '#type' => 'textfield',
        '#title' => t('Twitter Username'),
        '#default_value' => variable_get('social_sliders_twit_username'),
    );


    return social_sliders_system_settings_form($form, FALSE);
}

