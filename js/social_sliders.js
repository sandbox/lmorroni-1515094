/*
 *jQuery calls to show/hide the sldiers on click
 */
jQuery(document).ready(function() {
    //facebook and twitter slide outs
    jQuery('#fbTab').toggle(function() {
        jQuery(this).parent().animate({
            right:'0px'
        }, {
            queue:false, 
            duration: 500
        });
        var docHeight = jQuery(document).height();
        jQuery('#fbSlideContent').height(docHeight);
    }, function() {
        jQuery(this).parent().animate({
            right:'-380px'
        },500, function () {
            jQuery('#fbSlideContent').css('height',100)
        });
    });

    jQuery('#twtTab').toggle(function() {
        jQuery(this).parent().animate({
            right:'0px'
        }, {
            queue:false, 
            duration: 500
        });
        var docHeight = jQuery(document).height();
        jQuery('#twtSlideContent').height(docHeight);
    }, function() {
        jQuery(this).parent().animate({
            right:'-280px'
        },500, function () {
            jQuery('#twtSlideContent').css('height',100)
        });
    });
    
});